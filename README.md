# cryptmachine

A small golang app to turn one word into another

Limited probably wont run if compiled on  windows without some small modifications, if you want to compile on windows its reccomended you change the /tmp/results.txt field into a path reachable on your OS

this is a demo, if you want to make this into something useable or available for general use I reccomend forking it or contributing.


A Demo API is available with the current dictionary, how to use it:

```
curl -X POST https://api.cryptmachine.com/rotor -d '{"date":"07042019","id":0,"msg":"hello world"}'
# Response
{"date":"07042019","id":0,"msg":"Lallies shackanite "}

curl -X POST https://api.cryptmachine.com/rotor -d '{"date":"07042019","id":1,"msg":"Lallies shackanite "}'
# Response 
{"date":"07042019","id":1,"msg":"hello world "}
```

### Contact details

cryptmachine@protonmail.com
