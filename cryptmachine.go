package main

import (
	"fmt"
	"./coders"
)




// use this with go generate to turn a txt file into a constant
//go:generate go run docs/includetxt.go



/*
testing data, if you want to use this in reality you will need to add some inputs

either with a http server :

https://stackoverflow.com/questions/15672556/handling-json-post-request-in-go

or just add some command line flags to the package

https://gobyexample.com/command-line-flags

*/
func main() {


	date := "07042019"

	i := `{"date":"`+ date +`","id":0,"msg":"hello test"}`

	j := `{"date":"`+ date +`","id":1,"msg":"`+ msgcode.MsgHandler(i) +`"}`

	fmt.Println(i)


	fmt.Println(msgcode.MsgHandler(i))

	fmt.Println(j)

	fmt.Println(msgcode.MsgHandler(j))


}

// The lambda code for the demo service deployment

/*


type Response events.APIGatewayProxyResponse


type Payload struct {
	Date string `json:"date"`
	Operation int `json:"id"`
	Payload string `json:"msg"`
}

func main() {
	lambda.Start(handleRequest)
}


func handleRequest(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {


	for key, value := range request.Headers {
		fmt.Printf("    %s: %s\n", key, value)
	}

	json_bytes := []byte(request.Body)

	PayloadunParse := Payload{}

	err := json.Unmarshal(json_bytes, &PayloadunParse)
	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	//D := strconv.Itoa(PayloadunParse.Date)
	O := strconv.Itoa(PayloadunParse.Operation)


	RespBody := `{"date":"`+ PayloadunParse.Date +`","id":`+ O +`,"msg":"`+ msgcode.MsgHandler(request.Body) +`"}`

	fmt.Printf("Processing request data for request %s.\n", request.RequestContext.RequestID)
	fmt.Printf("Body size = %d.\n", len(request.Body))
	fmt.Println(request.Body)

	return events.APIGatewayProxyResponse { Body: RespBody, StatusCode: 200 }, nil
}
}
*/


